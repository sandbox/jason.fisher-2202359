Disclaimer:
This module has been made to ease the activities of custom styling for user-facing content forms.
A site-builder's theming kit can be made by adding the css_injector and field_formatter_class modules to this one.  Such a configuration would allow the tagging and styling of cck objects anywhere they appear, all from configuration pages without needing server access.

Eventually, I will consider forming such a package or folding those modules into this one, taking the best parts of each, and making a super builder's theme tool.  Right now I just want to reduce the manhours for styling large content forms.

This module was built with heavy code guidance from references_id_trim.module and some help from the #drupal-support irc.
I have not yet tested this module on field collections or other newer/obscure entities.  I expect this crashes with field collections at this time.

Installation:
1. Unpack module into module folder
2. Enable on module page
3. Go to a field settings page, [site]/admin/structure/types/manage/[type]/fields/[field] and scroll to the bottom.
4. Enter a comma separated list of css classes in the 'Add These Classes...' field 
5. Enter some css styling for those classes in your style sheets or into the css_inkector

Intented Use:
  My company is building several sites with field-intensive, user-facing content forms.  These forms are often required to stylistically match an arbitrary standard.  Finding the right combinations of fieldsets and descriptors for css was taking too much time.
  I had found modules that do this for displays already, field_formatter_class and field_formatter_css_class, but I did not find one for the form side of things.  This module is intended to mend that shortfall.


